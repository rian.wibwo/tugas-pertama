import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  get child => null;
  int number = 0;

  void kurangiTombol() {
    setState(() {
      number = number - 1;
    });
  }

  void tambahTombol() {
    setState(() {
      number = number + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Tugas Pertama'),
        leading: Image.asset('assets/images/user.png'),
        actions: const [Icon(Icons.search), Icon(Icons.notifications)],
        backgroundColor: Colors.green,
      ),
      body: Column(
        children: [
          const SizedBox(height: 20),
          const Text(
            'Sparkle From The East',
            style: TextStyle(fontFamily: "lobs", fontSize: 30),
          ),
          const SizedBox(height: 10),
          Container(
            height: 300,
            width: double.infinity,
            margin: const EdgeInsets.symmetric(horizontal: 16),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16),
            ),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(16),
                child:
                    Image.asset('assets/images/news.jpeg', fit: BoxFit.cover)),
          ),
          const SizedBox(height: 10),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 16),
            child: const Text(
              'As in history, the material in hand remains silent if no questions are asked. The nature of these questions depends on the school to which the geologist belongs and on the objectivity of his investigations, Hans Cloos called this way of interrogation the dialogue with the earth (R.W. VAN BEMMELEN)',
              textAlign: TextAlign.justify,
            ),
          ),
          const SizedBox(height: 50),
          const Text("Who's like"),
          Text(number.toString(),
              style: TextStyle(fontSize: 10 + number.toDouble())),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ElevatedButton(
                    onPressed: kurangiTombol,
                    child: const Text('Dislike'),
                    style: ElevatedButton.styleFrom(primary: Colors.green)),
                ElevatedButton(
                    onPressed: tambahTombol,
                    child: const Text('Like'),
                    style: ElevatedButton.styleFrom(primary: Colors.green)),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
